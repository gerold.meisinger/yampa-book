=============
Book of Yampa
=============

*Learn yourself a Yampa for great good!*

.. toctree::
  :maxdepth: 2
  :caption: Contents:

  intro
  embed
  reactimate
  yampa
  ui
  cheatsheet
  links
  faq
  glossary
  contribute

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
