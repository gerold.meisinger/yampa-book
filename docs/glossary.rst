########
Glossary
########

Haskell Base
============

.. glossary::
  :sorted:

  Arrow
    short description okay, links and papers

  Applicative
    just links and papers

  lifting
    lifting a function to a Monad, lifting a Monad down the stack, lifting a Monad to MSF

  widening
    Arrow widening

  Functor
    just links and papers

  Monad
    just links and papers

  Monad Transformer
    just links and papers

  Monadic Streams
    TODO

  MSF
    see :term:`Monadic Stream Function`

  Monadic Stream Function
    see :term:`Dunai`, [FrpRefac16]_
    

Terminology
===========

.. glossary::
  :sorted:

  domain-specific language
    TODO

  DCTP
    see :term:`denotative continuous time programming`

  causal
    TODO

  circuit network
    see :term:`signal network`

  dynamic structure
    it's one thing to run a fixed sized list of signals all behaving the same but another to make it change dynamically and with changing behaviours

  Behaviour
    TODO

  Event
    TODO

  state
    building state up over time, not to be confused with State monad

  space-leak
    also see :term:`time-leak`

  simulation, deterministic
    TODO

  simulation, non-deterministic
    TODO

  temporal
    TODO

  time, continuous
    `Conal Elliott - Why program with continuous time? <http://conal.net/blog/posts/why-program-with-continuous-time>`_

  time, discrete
    TODO

  hybrid system
    TODO

  time, hybrid
    TODO

  time-leak
    also see :term:`space-leak`

  denotative continuous time programming
    `Stackoverflow.com - Specification for a Functional Reactive Programming language (2011) by Conal Elliott <https://stackoverflow.com/questions/5875929/specification-for-a-functional-reactive-programming-language/5878525#5878525>`_: "I'm glad you're starting by asking about a specification rather than implementation first. There are a lot of ideas floating around about what FRP is. ..."

  reactive programming
    TODO: links to papers

  functional reactive programming, pull-based
    TODO: links to papers

  functional recative programming, push-based
    TODO: links to papers

  functional reactive programming, arrowized
    TODO: links to papers

  AFRP
    see :term:`functional reactive programming, arrowized`

  CFRP
    see :term:`functional reactive programming, classic`

  functional reactive programming, classic
    TODO: links to papers

  programming paradigm
    Roy, Peter Van und Seif Haridi: Concepts, Techniques, and Models of Computer Programming. MIT Press, 2004.

  imperative programming
    TODO

  procedural programming
    TODO

  object-oriented programming
    TODO

  FP
    see :term:`functional programming`

  functional programming
    TODO

  FRP
    see :term:`functional reactive programming`

  functional reactive programming
    `Stackoverflow.com - What is (functional) reactive programming? (2009) by Conal Elliott <https://stackoverflow.com/a/1030631/392549>`_

    [FrpExt17]_ 3.3.2 "FRP tries to shift the direction of data-flow from message passing to data dependency. This helps reason about what things are over time, as opposed to how changes propagate."

Yampa
=====

.. glossary::

  embedding
    TODO

  reactimating
    TODO

  sense
    TODO

  actuate
    TODO

  wormholes
    TODO

  white hole
    TODO

  black hole
    TODO

  Switch
    TODO

  Signal
    TODO

  SF
    see :term:`Signal Function`

  Signal Function
    TODO

  Signal Network
    TODO: image

Implementations
===============

.. glossary::
  :sorted:

  Clean
    FRP implementation

  Unity
    Unity3D game engine

  RT-FRP
    realtime, FRP implementation

  E-FRP
    eventbased, FRP implementation

  Yampa
    old Yampa, compare with BearRiver and Dunai

  Elm
    [FrpRefac16]_ 3.3.2 Limitations of FRP and Arrowized FRP: "Some FRP and FRP-inspired implementations and languages offer mechanisms to work around this problem. Elm [83], for instance, offers handles to push specific changes onto widgets, thus helping to break cycles involving interactive visual elements."

  Reactive Banana
    [FrpRefac16]_ 3.3.2 Limitations of FRP and Arrowized FRP: "Some FRP and FRP-inspired implementations and languages offer mechanisms to work around this problem... Reactive Banana offers sinks for each WX widget property, to which a signal can be attached. These mechanisms are not general solutions applicable to every reactive element, but ad hoc solutions to enable pushing changes to specific kinds of resources."

  Esterel
    [FrpRefac16]_ 11.1 Related Work - MSFs: "Esterel is a synchronous data-flow programming language with support for concurrency and signal inhibition. Esterel rejects programs that give multiple values to the same signal at the same sampling time. Our framework support classic FRP and, in principle, signals are defined once for all time. Signals in Esterel can be broadcasted across the whole program from any point. This form of broadcasting might be implementable with MSFs by means of a State monad. However, Esterel provides additional static guarantees, for example, that a broadcasted signal only has one value per cycle, and this could only be detectable during runtime with the approach based on MSF’s mentioned above."

  Lustre
    [FrpRefac16]_ 11.1 Related Work - MSFs: "Lustre is a synchronous data-flow programming language that has been used in aerospace, transportation, nuclear power plants and wind turbines, among many others. Lustre programs are defined in terms of flows (streams) and nodes (causal stream functions). Lustre’s type system includes both information about the amount of history examined of each flow examined by each node, and clocking rates at which each flow is being produced. Together with Lustre’s clock calculus, this helps guarantee that all flows are well-formed (always defined). MSFs do not have any notion of clocks or clocking rate, making it, in principle, harder to capture those constraints and ensure the same kinds of guarantees. MSFs are defined and combined using a series of combinators that ensure that all values are well-typed. Nevertheless, our language is embedded in Haskell, which allows representing bottoms, so a program written using MSFs may not be guaranteed to be productive (simply because the programmer may have used non-terminating Haskell expressions)."

  Lucid Synchrone
    not to be confused with :hackage:lucid DSL for HTML

    [FrpRefac16]_ 11.1 Related Work - MSFs: "Lucid Synchrone is a programming language inspired by Lustre, which extends it with high-level concepts from functional programming, a richer type language, and a more versatile and usable clock system. Types in Lucid Synchrone are polymorphic, and the type system supports type inference. This approach is closer to our MSFs, due to the richness of the language, although MSFs do not explicitly support clocks or include any kind of clock calculus. Like in the case of Lustre, the language of MSFs is bigger than that of Lucid Synchrone nodes: constructions that would be rejected by Lucid Synchrone’s compiler might be accepted by the Haskell compiler if described using MSF. This makes these languages, in principle, safer than full MSFs, unless the"
    
  Netwire
    FRP library
     http://hub.darcs.net/ertes/netwire.

  FRPNow!
    FRP library

  Euterpea
    compare with Yampa

  BearRiver
    new Yampa, compare with Dunai and Yampa

  Dunai
    compare with Yampa and BearRiver
