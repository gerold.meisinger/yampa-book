#####
Yampa
#####

Quickstart
==========

.. include:: wrapup_yampa.rst

Accumulating state
==================

The point of this example is: time is entered as input, but keyboard presses are also entered as input.

.. literalinclude:: ../src/state.hs
  :emphasize-lines: 30

:download:`state.hs <../src/state.hs>`

>>> cabal repl state # --ghci-options '+RTS --io-manager=native -RTS'
>>> main

Stateful functions
==================

* constant
* arr sin
* time
* count
* integral
* sumS
* feedback 0 (arr feedbackAdd
* stateful 0 statefulAdd
* hold 0
* accumHoldBy accumAdd 0

Animation
=========

The point of this example is: it doesn't matter how sophisticated the rendering will be in the end, we can define a basic animation function. It doesn't matter if we are using an ASCII renderer, 2D pixels, 2D vectors or an 3D renderer (which is still further abstracted into OpenGL, Direct3D and Vulkan and makes everything complicated).

.. literalinclude:: ../src/animation.hs
  :emphasize-lines: 35, 40

:download:`animation.hs <../src/animation.hs>`

>>> cabal repl animation # --ghci-options '+RTS --io-manager=native -RTS'
>>> main

Refinement
----------

Now we have several options for animations:

Use arrow combinators with time and speed up the animation by 5

.. code::

  process = proc a -> do
    frame <- (\t -> ball !! (floor (5.0 * t) `mod` length ball)) ^<< time -< ()
    ...

Extract the animation logic into several functions which allows us to reuse it however we want:

.. code::

  animate :: [a] -> Double -> SF Identity () a
  animate frames speed = constant speed >>> integral >>^ getFrame frames
  --animate frames speed = time >>^ (* speed) >>^ getFrame frames

  getFrame :: [a] -> Double -> a
  getFrame frames t = let n = length frames in frames !! (floor t `mod` n)

  process = proc a -> do
    frame <- animate ball 5.0 -< ()
    ...

Movement
========

.. literalinclude:: ../src/objmove.hs
  :emphasize-lines: 42-47

:download:`objmove.hs <../src/objmove.hs>`

>>> cabal repl objmove # --ghci-options '+RTS --io-manager=native -RTS'
>>> main

Refinement
----------

If you want to add bounds to the position you could write:

.. code::

  object = proc a -> do
    x <- accumHoldBy (\old dir -> max 0 . min 30 $ old + dir) 0 -< case a of Just 'd' -> Event 1; Just 'a' -> Event (-1); _ -> NoEvent
    y <- accumHoldBy (\old dir -> max 0 . min 10 $ old + dir) 0 -< case a of Just 's' -> Event 1; Just 'w' -> Event (-1); _ -> NoEvent
    ...

Here are some other animations

.. code::

  stick = "-/|\\"
  emoji = "😃😄😃😃"
  clock = "🕐🕑🕒🕓🕔🕕🕖🕗🕘🕙🕚🕛"

Recursive states
================

.. literalinclude:: ../src/spring.hs
  :emphasize-lines: 43-48

:download:`spring.hs <../src/spring.hs>`

>>> cabal repl spring # --ghci-options '+RTS --io-manager=native -RTS'
>>> main

.. code::

  -- | Well-formed looped connection of an output component as a future input.
  feedback :: Monad m => c -> MSF m (a, c) (b, c) -> MSF m a b
  feedback c sf = MSF $ \a -> do
    ((b', c'), sf') <- unMSF sf (a, c)
    return (b', feedback c' sf')

Switching behaviour
===================

=========================== ============= ==============
type                        immediate     delayed
=========================== ============= ==============
once                        ``switch``    ``dSwitch``
recurring                   ``rSwitch``   ``drSwitch``
parallel using broadcasting ``pSwitchB``  ``dpSwitchB``
                            ``rpSwitchB`` ``drpSwitchB``
parallel using routing      ``pSwitch``   ``dpSwitch``
                            ``rpSwitch``  ``drpSwitch``
continuation                ``kSwitch``   ``kdSwitch``
=========================== ============= ==============

.. figure:: media/yampa_switch_.svg
  :align: center

  switch

.. figure:: media/yampa_pSwitchB_.svg
  :align: center

  pSwitchB

.. figure:: media/yampa_pSwitch_.svg
  :align: center

  pSwitch

`Reddit - How to read Yampa diagram? <https://www.reddit.com/r/haskell/comments/3jqfh7/how_to_read_yampa_diagram/curnksm/?utm_source=reddit&utm_medium=web2x&context=3>`_

These diagrams were originally designed by the author of this book. If you understand german you can read a description of ``switch``, ``pSwitchB`` and ``pSwitch`` here: [GamArchYam10]_

.. todo:: add english translation of switching diagrams from master thesis
