#####
Links
#####

Main Yampa papers
=================

.. [YamCade03] `The Yampa Arcade <https://www.antonycourtney.com/pubs/hw03.pdf>`_ (2003) by Antony Courtney, Henrik Nilsson, John Peterson `(archived) <https://web.archive.org/web/20140422070221/http://antonycourtney.com/pubs/hw03.pdf>`__

Simulated worlds are a common (and highly lucrative) application domain that stretches from detailed simulation of physical systems to elaborate video game fantasies. We believe that Functional Reactive Programming (FRP) provides just the right level of functionality to develop simulated worlds in a concise, clear and modular way. We demonstrate the use of FRP in this domain by presenting an implementation of the classic “Space Invaders” game in Yampa, our most recent Haskell-embedded incarnation of FRP.

.. [FrpRefac16] `Functional Reactive Programming, Refactored <http://www.cs.nott.ac.uk/~psxip1/papers/2016-HaskellSymposium-Perez-Barenz-Nilsson-FRPRefactored-short.pdf>`_ (2016) by Ivan Perez, Manuel Bärenz, Henrik Nilsson

Functional Reactive Programming (FRP) has come to mean many things. Yet, scratch the surface of the multitude of realisations, and there is great commonality between them. This paper investigates this commonality, turning it into a mathematically coherent andpractical FRP realisation that allows us to express the functionality of many existing FRP systems and beyond by providing a minimal FRP core parametrised on a monad. We give proofs for our theoretical claims and we have verified the practical side by benchmarking a set of existing, non-trivial Yampa applications running on top ofour new system with very good results.

.. [FrpExt17] `Extensible and Robust Functional Reactive Programming <http://www.cs.nott.ac.uk/~psxip1/papers/2017-Perez-thesis-latest.pdf>`_ (2017) by Iván Pérez Domínguez, MSc (`First Year Report <http://www.cs.nott.ac.uk/~psxip1/papers/2014-Perez-1st-year-report.pdf>`__)

Programming GUI and multimedia in functional languages has been a long-term challenge, and no solution convinces the community at large. Purely functional GUI and multimedia toolkits enable abstract thinking, but have enormous maintenance costs. General solutions like Functional Reactive Programming present a number of limitations. FRP has traditionally resisted efficient implementation, and existing libraries sacrifice determinism and abstraction in the name of performance. FRP also enforces structural constraints that facilitate reasoning, but at the cost of modularity and separation of concerns. This work addresses those limitations with the introduction of Monadic Stream Functions, anextension to FRP parameterised over a monad. I demonstrate that, in spite of being simpler thanother FRP proposals, Monadic Stream Functions subsume and exceed other FRP implementations. Unlike other proposals, Monadic Stream Functions maintain purity at the type level, whichis crucial for testing and debugging. I demonstrate this advantage by introducing FRP testingfacilities based on temporal logics, together with debugging tools specific for FRP. I present two uses cases for Monadic Stream Functions: First, I show how the new constructsimproved the design of game features and non-trivial games. Second, I present Reactive Valuesand Relations, an abstraction for model-view coordination in GUI programs based on a relationallanguage, built on top of Monadic Stream Functions. Comprehensive examples are used to illustrate the benefits of this proposal in terms of clarity, modularity, feature coverage, and its low maintenance costs. The testing facilities mentioned before are used to encode and statically checkdesired interaction properties.

All Yampa papers
================

Sorted newest first:

.. [TestDebugFrp17] `Testing and Debugging Functional Reactive Programming <http://www.cs.nott.ac.uk/~psxip1/papers/2017-ICFP-Perez-Nilsson-TestingAndDebuggingFRP-latest.pdf>`_ (2017) by Ivan Perez, Henrik Nilsson

.. [CcaRevisit16] `Causal Commutative Arrows Revisited <https://www.cl.cam.ac.uk/~jdy22/papers/causal-commutative-arrows-revisited.pdf>`_ (2016) by Jeremy Yallop, Hai Liu

.. [SetNonInfFrp14] `Settable and Non-Interfering Signal Functions for FRP <https://www.danwc.com/data/nichoiceICFP2014.pdf>`_ (2014) by Daniel Winograd-Cort, Paul Hudak `(more) <https://www.danwc.com/research>`__

.. [SwitchOn08] `Switched-On Yampa <http://www.cs.nott.ac.uk/~psznhn/Publications/padl2008.pdf>`_ (2008) by George Giorgidze, Henrik Nilsson

.. [DynInterVR08] `Dynamic, Interactive Virtual Environments <https://web.archive.org/web/20110525110046if_/http://imve.informatik.uni-hamburg.de/files/71-Blom-Diss-online.pdf>`_ (2008) by Kristopher James Blom

.. [Fp3dGames05] `Functional Programming and 3D Games <https://web.archive.org/web/20060510054316/http://www.cse.unsw.edu.au/%7Epls/thesis/munc-thesis.pdf>`_ (2005) by Mun Hon Cheong

.. [ArrRobFrp03] `Arrows, robots, and Functional Reactive Programming <http://www.cs.nott.ac.uk/~psznhn/Publications/afp2002.pdf>`_ (2003) by Paul Hudak, Antony Courtney, Henrik Nilsson, John Peterson

.. [FrpCont02] `Functional Reactive Programming, Continued <https://www.antonycourtney.com/pubs/frpcont.pdf>`_ (2002) by Henrik Nilsson, Antony Courtney and John Peterson

* `Wormholes: Introducing Effects to FRP <https://www.danwc.com/data/Winograd-Cort-Wormholes.pdf>`_ (2012) by Daniel Winograd-Cort, Paul Hudak `(video) <https://www.youtube.com/watch?v=KZewVsyTw60>`__
* `Virtualizing Real-World Objects in FRP <https://www.danwc.com/data/VROinFRP-TR1446.pdf>`_ (2011) by Daniel Winograd-Cort, Hai Liu, Paul Hudak
* Causal commutative arrows and their optimization (2009) by Hai Liu, Eric Cheng, and Paul Hudak
* Demo-outline: Switched-on Yampa: Programming Modular Synthesizers in Haskell (2007) by George Giorgidze, Henrik Nilsson
* Plugging a space leak with an arrow (2007) by Hai Liu, Paul Hudak
* Dynamic optimization for functional reactive programming using generalized algebraic data types (2005) by Henrik Nilsson

Non-english Yampa papers
========================

German
------

.. [GamArchYam10] `Game-Engine-Architektur mit funktional-reaktiver Programmierung in Haskell/Yampa <https://web.archive.org/web/20120426195141/http://theses.fh-hagenberg.at/system/files/pdf/Meisinger10.pdf>`_ (2010) von Gerold Meisinger

.. [VisIdeAfrp07] `Visuelle Entwicklungsumgebung zur Erzeugung von Haskell AFRP Code <https://imve.informatik.uni-hamburg.de/projects/VisualAFRP>`_ (2007) von Piotr Szal

FRP papers
==========

Sorted newest first:

.. [MathPropMSF16] `Mathematical Properties of Monadic Stream Functions <http://cs.nott.ac.uk/~ixp/papers/msfmathprops.pdf>`_ (2016) by Manuel Bärenz, Ivan Perez, Henrik Nilsson

.. [SurvFrp10] `A Survey of Functional Reactive Programming <http://www.cs.rit.edu/~eca7215/frp-independent-study/Survey.pdf>`_ (2010) by Edward Amsden `(archived) <https://web.archive.org/web/20120406012927/http://www.cs.rit.edu/~eca7215/frp-independent-study/Survey.pdf>`__

.. [PushPullFrp09] `Push-pull functional reactive programming <http://conal.net/papers/push-pull-frp/push-pull-frp.pdf>`_ (2009) by Conal Elliott `(video) <http://vimeo.com/6686570>`__ `(slides) <http://conal.net/papers/push-pull-frp/push-pull-frp-slides.pdf>`__

.. [SafeFrpDep09] `Safe functional reactive programming through dependent types <https://core.ac.uk/download/pdf/21172885.pdf>`_ (2009) by Neil Sculthorpe, Henrik Nilsson `(video) <http://vimeo.com/6632457>`__

.. [GenuineUI01] `Genuinely Functional User Interfaces <http://conal.net/papers/genuinely-functional-guis.pdf>`_ (2001) by Antony Courtney and Conal Elliott

* Back to the Future: Time Travel in FRP (2017) by Ivan Perez
* Rhine - frp with type-level clocks (2016) by Manuel Bärenz
* Higher-order functional reactive programming without spacetime leaks (2013) by N. R. Krishnaswami
* Push-Pull Signal-Function Functional Reactive Programming (2012) by E. Amsden
* Higher-order functional reactive pro- gramming in bounded space (2012) by N. R. Krishnaswami, N. Benton, J. Hoffmann
* Improving Push-based FRP (2008) by W. Jeltsch
* E-FRP with priorities (2007) by R. Kaiabachev, W. Taha, A. Zhu
* Plugging a space leak with an arrow (2007) by Hai Liu, Paul Hudak
* Functional Reactive Programming for Real-Time Reactive Systems (2002) by Wan, Zhanyong
* Event-driven FRP (2002) by Z. Wan, W. Taha, P. Hudak
* Real-time FRP (2001) by Z. Wan, W. Taha, and P. Hudak
* Functional reactive programming from first principles (2000) by Zhanyong Wan, Paul Hudak
* Functional reactive animation (1997) by Conal Elliott, Paul Hudak
* `Reification of time in FRP <http://pchiusano.blogspot.co.uk/2010/07/reification-of-time-in-frp-is.html>`_
* Functional reactive programming for real-time reactive systems (2002) by Zhanyong Wan

* Fault tolerant functional reactive programming (2018) by Ivan Perez

.. todo:: split up technical papers and tutorials

Other papers
============

Sorted oldest first:

.. [WhyFP90] `Why functional programming matters <https://www.cs.kent.ac.uk/people/staff/dat/miranda/whyfp90.pdf>`_ (1990) by John Hughes

.. [MonadFP95] `Monads for functional programming <https://homepages.inf.ed.ac.uk/wadler/papers/marktoberdorf/baastad.pdf>`_ (1995) Philip Wadler

..
  .. [GenMonArr98] `Generalising monads to arrows <https://www.cse.chalmers.se/~rjmh/Papers/arrows.pdf>`_ (1998) by John Hughes

.. [GenMonArr00] `Generalising monads to arrows <https://www.sciencedirect.com/science/article/pii/S0167642399000234/pdfft>`_ (2000) by John Hughes

.. [NewNotatArr01] `A New Notation for Arrows <https://www.staff.city.ac.uk/~ross/papers/notation.html>`_ (2001) by Ross Paterson

.. [AllMonads03] `All About Monads <https://wiki.haskell.org/All_About_Monads>`_ (2003) Jeff Newbern

.. [ArrComp03] `Arrows and Computation <https://www.staff.city.ac.uk/~ross/papers/fop.html>`_ (2003) by Ross Paterson

.. [ProgArr05] `Programming with Arrows <https://www.cse.chalmers.se/~rjmh/afp-arrows.pdf>`_ (2005) by John Hughes

.. [AppProg08] `Applicative Programming with Effects <http://www.staff.city.ac.uk/~ross/papers/Applicative.html>`_ (2008) by Conor McBride, Ross Paterson

* Notions of computation and monads (1991) by Moggi
* Comprehending monads (1992) by Philip Wadler

.. todo:: where is the 2000 version of Generalising monads to arrows online?

Codedocs
========

* :hackage:`Hackage Dunai <dunai>`

  * :hackage:`Hackage Dunai - Core <dunai/docs/Data-MonadicStreamFunction-Core.html>`

* :hackage:`Hackage BearRiver <bearriver/docs/FRP-BearRiver.html>`
* :hackage:`Hackage Yampa <Yampa/docs/FRP-Yampa.html>` (old Yampa!)

Repos
=====

* https://github.com/ivanperez-keera/Yampa

  * https://github.com/keera-studios/keera-hails/tree/develop/keera-hails-reactive-yampa

* https://github.com/ivanperez-keera/dunai

  * https://github.com/ivanperez-keera/dunai/tree/develop/dunai-frp-bearriver

* https://github.com/turion/rhine

Examples
========

* `SpaceInvaders <https://github.com/ivanperez-keera/SpaceInvaders>`_
* `Haskanoid <https://github.com/ivanperez-keera/haskanoid>`_
* `Bearriver Arcade <https://github.com/walseb/The_Bearriver_Arcade>`_
* :hackage:`Pang-a-Lambda <pang-a-lambda-0.2.0.0/src/>`
* `e1338  <https://github.com/madjestic/e1338>`_
* `2048 <https://github.com/Jyothsnasrinivas/eta-android-2048>`_
* `MandelbrotYampa <https://github.com/madjestic/Haskell-OpenGL-Tutorial/tree/master/MandelbrotYampa>`_

.. todo:: include from https://github.com/ivanperez-keera/Yampa/

Tutorials
=========

* `Yampy Cube by Konstantin Zudov at Helsinki FRP Meetup May 6, 2015 (video) <https://www.youtube.com/watch?v=T7XwTolu9YI>`_
* `Brief Introduction to Functional Reactive Programming and Yampa <http://www.cs.nott.ac.uk/~psznhn/FoPAD2007/Talks/nhn-FoPAD2007.pdf>`_ (2007) Henrik Nilsson
* `wiki.haskell.org Yampa <https://wiki.haskell.org/Yampa>`_ (old Yampa)

  * `wiki.haskell.org Yampa/reactimate <https://wiki.haskell.org/Yampa/reactimate>`_ (old Yampa)
* `The Yampa Arcade - Slides <http://www.cs.nott.ac.uk/~psznhn/Talks/HW2003-YampaArcade.pdf>`_ (archived: `[1] <https://web.archive.org/web/20170812235651/http://www.cs.nott.ac.uk/~psznhn/Talks/HW2003-YampaArcade.pdf>`_ (old Yampa)
* `Yampa, Arrows, and Robots by Paul Hudak <http://www.cs.yale.edu/homes/hudak/CS429F04/LectureSlides/YampaForCs429.ppt>`_ (old Yampa)
* `Keera Studios Blog <https://keera.co.uk/all-posts>`_

  * `Building a reactive calculator in Haskell  <https://keera.co.uk/2020/05/28/building-a-reactive-calculator-in-haskell-1-5>`_

Old Yampa Examples
==================

* https://github.com/pedromartins/cuboid

	* https://www.youtube.com/watch?v=-IpE0CyHK7Q

* https://github.com/helsinki-frp/yampy-cube
* https://github.com/werk/YampaShooter

* `Vehicle Platooning Simulations with Functional Reactive Programming <http://www.cs.yale.edu/homes/piskac/papers/2017FinkbeinerETALTORCS.pdf>`_
* `Stackoverflow - Is there a way to create a Signal Function out of getLine in Yampa using reactimate <https://stackoverflow.com/questions/68589984>`_
* `Stackoverflow - N-body with Yampa FRP, haskell <https://stackoverflow.com/questions/65609361>`_

Libraries
=========

* `Euterpea <https://www.euterpea.com>`_
* reactive-banana
* elera
* elm
* TODO

Books
=====

.. [HaskExpr00] `The Haskell School of Expression <http://www.cs.yale.edu/homes/hudak/SOE>`_ (2000) by Paul Hudak

.. [HaskHiPerf16] `Haskell High Performance Programming - Chapter 13: Functional Reactive Programming <https://subscription.packtpub.com/book/application-development/9781786464217/13>`_ (2016) by Samuli Thomasson

.. [ManningFrp16] `Functional Reactive Programming <https://www.manning.com/books/functional-reactive-programming>`_ (2016) by Stephen Blackheath, Anthony Jones

.. [GameProgHask15] `Game programming in Haskell <https://leanpub.com/gameinhaskell>`_ (2015) by Elise Huard

..
  I only want to include books which are relevant to the topic, which is: FRP and programming interactive applications (e.g. games) in Haskell.
  This should NOT turn into a list of ALL introductory texts on Haskell.
  LYAH is listed here because I personally found it to be the best introduction and I want to provide readers with ONE specific entry point.

.. [LearnGood11] `Learn You A Haskell For Great Good! <http://learnyouahaskell.com>`_ (2011) by Miran Lipovaca

TODO
====

* Bridging the GUI Gap with Reactive Values and Relations (2005) by Ivan Perez, Henrik Nilsson
* Fault-Tolerant Swarms
* `The essence and origins of FRP <https://github.com/conal/talk-2015-essence-and-origins-of-frp>`_ (2015) by Conal Elliott
* `FRP Zoo <https://github.com/gelisam/frp-zoo>`_
* `Controlling Time and Space: understanding the many formulations of FRP" <https://www.youtube.com/watch?v=Agu6jipKfYw>`_ (2014) by Evan Czaplicki
* Imperative functional programming?
* https://www.youtube.com/watch?v=1MNTerD8IuI
* Wayward Tide was written in Haskell?
* https://notebook.readthedocs.io/en/latest/haskell/learn-you-a-haskell/chapter-2.html
* B. Victor, “Inventing on Principle.” http://vimeo.com/36579366, 2011
* Declarative Game Programming (2014) by Henrik Nilsson, Ivan Perez `(slides) <http://www.cs.nott.ac.uk/~psznhn/Talks/PPDP2014-tutorial.pdf>`__
* Roy, Peter Van und Seif Haridi: Concepts, Techniques, and Models of Computer Programming. MIT Press, 2004.
* Asynchronous functional reactive programming for GUIs (2013) by E. Czaplicki and S. Chong
* A survey on reactive programming by E. Bainomugisha, A. L. Carreton, T. v. Cutsem, S. Mostinckx, and W. d. Meuter
* https://imve.informatik.uni-hamburg.de/files/116-blom_beckhaus_DIVE_VR.pdf
* https://imve.informatik.uni-hamburg.de/files/55-SEARISworkshop_DIVEs-FRVR.pdf
* `Dunai Issue 236 - Bearriver: ArrowLoop yampa and bearriver differences <https://github.com/ivanperez-keera/dunai/issues/236>`_
* `Dunai Issue 245 - using embed with a constant 1.0 MSF to get (printable) results <https://github.com/ivanperez-keera/dunai/issues/246>`_
* `Dunai Issue 174 - Space Leak <https://github.com/ivanperez-keera/Yampa/issues/174>`_
* `Cogs and Levers - Functional Reactive Programming with Yampa <https://tuttlem.github.io/2014/07/31/functional-reactive-programming-with-yampa.html>`_
