############
Contributing
############

Comment using issues
====================

You can also add comments with `gitlab issues <https://gitlab.com/gerold.meisinger/yampa-book/-/issues>`_.

Fork GIT and create pull request
================================

Fork the git repository, change the text and create a pull requests.

  >>> git clone https://gitlab.com/gerold.meisinger/yampa-book

If you want to build the docs to see the final result before sending pull requests you need to install the sphinx documentation generator.

* Install `python3 <https://www.python.org>`_

>>> apt install python3 python3-pip

* Install sphinx

>>> pip install sphinx sphinx-rtd-theme sphinx-comments

* Build the docs

>>> cd docs
>>> make html

If you want to edit SVGs they use the `Computer Modern Unicode font <https://www.ctan.org/pkg/cm-unicode>`_ which is also bundled with some Latex distributions and is used in a lot of scientific papers.

Windows: install manually from link

Linux:

>>> apt install cm-super

Troubleshooting
---------------

.. code:: none

  Could not install docutils: run pip install pip --upgrade manually

``apt install python3-pip``

Housekeeping tasks
==================

Some housekeeping task everyone can contribute without knowledge on Haskell and Yampa:

* Correct typos
* Rewrite sentences with proper english (I'm not native speaker after all)
* Research links (papers, tutorial, examples etc.) and add RST markup
* Retrieve archive.org wayback machine versions for important links (papers) to make them permanent
* Fill in the glossary terms with description and citations from papers where the terms are mentioned
* Record screenshots and gif animations of graphical example outputs
* Work on todo list (see below)
* Run und test examples, add comments
* Add or answer :doc:`FAQs <faq>`
* Answer `issues <https://gitlab.com/gerold.meisinger/yampa-book/-/issues>`_

Add content
===========

* See `roadmap <https://gitlab.com/gerold.meisinger/yampa-book/-/blob/main/README.md>`_ in readme
* Good enough is good, better to have more content
* Keep the examples as simple as possible and self-contained

  * Is it really necessary to start up an SDL or OpenGL context to talk about animation or is the console with ASCII art sufficient
  * Is it really necessary to use a 3 dimensional vector to talk about stateful positions or is 1 dimension sufficient
  * Is it really necessary to use point-free super sections or is a verbose lambda with explicit type signature more comprehendable

* Scope

  * Yampa FRP meaning everything based on Dunai, BearRiver, Yampa (old), Euterpea(?), Rhine(?). It's okay to add comparisons to other libraries like Reactive-Banana, Elm, Elera etc. though.
  * Don't teach fundamentals about Haskell, Monads, computer game programming, computer graphics, physics programming etc.. It's okay to recall concepts required for a specific example.

Restructured Text
=================

* https://bashtage.github.io/sphinx-material/rst-cheatsheet/rst-cheatsheet.html
* https://ghc.gitlab.haskell.org/ghc/doc/users_guide/editing-guide.html
* https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html
* https://docutils.sourceforge.io/docs/ref/rst/directives.html
* https://sublime-and-sphinx-guide.readthedocs.io/en/latest/references.html

Use doctext ``>>>`` syntax for bash and ghci examples because it doesn't get marked when copying to clipboard and can be pasted as-is without changes.

doctext:

>>> echo hello world
# hello world

codeblock:

.. code:: bash

  $ echo hello world
  # -bash: $: command not found

Todos
=====

.. todolist::
