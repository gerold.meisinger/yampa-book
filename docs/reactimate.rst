############
Reactimation
############

Basic reactimation in BearRiver Yampa
=====================================

Type signatures of ``reactimate``

.. code::

  reactimate
    :: Monad m =>
       m in                              -- inputInit/senseInit
       -> (Bool -> m (DTime, Maybe in))  -- input/sense
       -> (Bool -> out -> m Bool)        -- output/actuate
       -> SF Identity in out             -- process/signal function
       -> m ()

Note that the input and output procedure also have an ``Bool`` parameter which is unused if you look at the implementation therefore we can just ignore it.

Here is a minimal implementation which just produces 1.0s time deltas independent of the real world time. So far it's just a deterministic simulation like ``embed`` and doesn't provide any utility until we get real time deltas.

.. code::

  main = do
    reactimate inputInit input output time
      where
        inputInit  = pure ()
        input  _   = pure (1.0, Just ())
        output _ o = print o >> pure False
  -- > 1.0
  -- > 2.0
  -- > 3.0
  -- > ...
  -- > Interrupted.

Type signature of ``time``:

.. code::

  time :: Monad m => SF m a Time

The first question that comes to mind is: How do we quit ``reactimate``? Well in this case we don't, so just press :kbd:`Control-c`. On Windows there is an `issue <https://gitlab.com/gerold.meisinger/yampa-book/-/issues/1>`_ which messes up the console once you press :kbd:`Control-c` within the app. So you probably have to restart ghci now otherwise you get garbeld output and strange error messages.

Here is a more complete example which implements a full game loop with real time deltas.
Note that in a real game loop you wouldn't write it this way because you would like to have a stable frame rate, make the delay dependent of missed time and account for skipped frames.
Also I don't know how precise ``Data.Time.Clock.getCurrentTime`` is. But let's keep it simple for now. On Windows, make sure you run this example without the ``--io-manager=native`` option, otherwise ``threadDelay`` hangs the execution.

.. literalinclude:: ../src/reactimate.hs

:download:`reactimate.hs <../src/reactimate.hs>`

..
  >>> stack ghci # DON'T USE io-manager=native HERE!
  >>> :l src/reactimate.hs

>>> cabal repl reactimate # DON'T USE io-manager=native HERE!
>>> main

Press :kbd:`Control-c` again to quit. On Windows you need to restart GHCI one more time.

Note that ``inputInit`` is only used as fallback here if input never produces anything. You can see that if you end input with:

.. code::

  input dtRef tInit _ = do
    ...
    pure (dt, if at < 3 then Nothing else Just (show now))

  -- > start...
  -- > now: inputInit integral: 0.0
  -- > now: inputInit integral: 1.0099473
  -- > now: inputInit integral: 2.0170455
  -- > now: 2021-09-12 09:42:59.1492352 UTC integral: 3.0270455
  -- > Interrupted.

On Windows make sure you use GHC 9 and start with ``--io-manager=native -RTS`` option, otherwise ``NoBuffering`` and ``getChar`` wont work:

  >>> cabal repl reactimate --ghci-options '+RTS --io-manager=native -RTS'

In this example we use a poor man's input handling with ``getChar`` which just reads one character from the console. Depending on how long the user waits we get the corrsponding time delta. Once you press :kbd:`Q` the app quits.

.. literalinclude:: ../src/input.hs
  :emphasize-lines: 16

:download:`input.hs <../src/input.hs>`

>>> cabal repl input #--ghci-options '+RTS --io-manager=native -RTS'
>>> main

Reactimate in Dunai
===================

type signatures of reactimate

.. code::

  -- BearRiver
  reactimate
    :: Monad m =>
       m in                              -- inputInit/senseInit
       -> (Bool -> m (DTime, Maybe in))  -- input/sense
       -> (Bool -> out -> m Bool)        -- output/actuate
       -> SF Identity in out             -- process/signal function
       -> m ()

.. code::

  -- Dunai
  reactimate :: Monad m => MSF m () () -> m ()

.. code::

  main = do
    hSetBuffering stdin LineBuffering -- important if you work on Windows
    putStrLn "Enter some words: "
    Dunai.reactimate (arrM (const getLine) >>> arr reverse >>> arrM putStrLn)
  -- > Enter some words:
  -- > hello
  -- > olleh
  -- > world
  -- > dlorw

Note that the functions ``arrM_`` and ``liftS`` mentioned on https://github.com/ivanperez-keera/dunai don't really exist.

From the paper:

.. code::

  liftLM :: (Monad m, Monad n) => (forall a . m a -> n a) -> MSF m a b -> MSF n a b

  liftST = liftLM . lift

From source:

.. code::

  -- | Lifts a monadic computation into a Stream.
  constM :: Monad m => m b -> MSF m a b
  constM = arrM . const

  -- | Apply a monadic transformation to every element of the input stream.
  -- Generalisation of 'arr' from 'Arrow' to monadic functions.
  arrM :: Monad m => (a -> m b) -> MSF m a b
  arrM = ...

  -- | Apply trans-monadic actions (in an arbitrary way).
  -- This is just a convenience function when you have a function to move across
  -- monads, because the signature of 'morphGS' is a bit complex.
  morphS :: (Monad m1, Monad m2)
        => (forall c . m1 c -> m2 c)
        -> MSF m1 a b
        -> MSF m2 a b
  morphS = ...

  -- | Lift inner monadic actions in monad stacks.
  liftTransS :: (MonadTrans t, Monad m, Monad (t m))
             => MSF m a b
             -> MSF (t m) a b
  liftTransS = morphS lift

Therefore::

  arrM_  => constM
  liftS  => arrM
  liftLM => morphS
  liftST => liftTransS

MyReactimate
============

Let's implement our own ``myreactimate``

.. literalinclude:: ../src/myreactimate0.hs

:download:`myreactimate0.hs <../src/myreactimate0.hs>`

>>> cabal repl myreactimate0 --ghci-options '+RTS --io-manager=native -RTS'
>>> main

Now if want to make the type signatures explicit

.. code::

  myreactimate :: IO (Double, a) -> (b -> IO ()) -> SF Identity a b -> IO ()
  myreactimate sense actuate sf = reactimate $ senseSF >>> sfIO >>> actuateSF
    where
      senseSF :: MSF IO () (Double, a)
      senseSF = constM sense
      actuateSF :: MSF IO b ()
      actuateSF = arrM actuate
      sfIO :: MSF IO (Double, a) b
      sfIO = morphS (pure . runIdentity) (runReaderS sf)

  -- Couldn't match type ‘b1’ with ‘b’
  -- Expected: MSF IO b1 ()
  -- Actual: MSF IO b ()
  -- ‘b1’ is a rigid type variable bound by
  -- the type signature for: actuateSF :: forall b1. MSF IO b1 ()

Thats because the variable ``b`` mentioned in the signature is different from the ``b`` mentioned in the signature of ``actuateSF`` (which is renamed to ``b1``). We would have to use ``ScopedTypeVariables`` and ``forall``, which says something like: "for the following block assume the variables to be the same."

.. code::

  {-# LANGUAGE ScopedTypeVariables #-}

  myreactimate :: forall a b. IO (Double, a) -> (b -> IO ()) -> SF Identity a b -> IO ()
  myreactimate sense actuate sf = reactimate $ senseSF >>> sfIO >>> actuateSF
    where
      senseSF :: MSF IO () (Double, a)
      senseSF = constM sense
      actuateSF :: MSF IO b ()
      actuateSF = arrM actuate
      sfIO :: MSF IO (Double, a) b
      sfIO = morphS (pure . runIdentity) (runReaderS sf)

Here is the version that Haskell Language Server suggested

.. code::

  {-# LANGUAGE RankNTypes, KindSignatures #-}

  myreactimate :: forall a b. IO (Double, a) -> (b -> IO ()) -> SF Identity a b -> IO ()
  myreactimate sense actuate sf = reactimate $ senseSF sense >>> sfIO sf >>> actuateSF actuate
    where
      senseSF :: forall (m :: * -> *) a. Monad m => m a -> MSF m () a
      senseSF s = constM s
      actuateSF :: forall (m :: * -> *) a b. Monad m => (a -> m b) -> MSF m a b
      actuateSF a = arrM a
      sfIO :: forall (m2 :: * -> *) r a b. Monad m2 => MSF (ReaderT r Identity) a b -> MSF m2 (r, a) b
      sfIO s = morphS (pure . runIdentity) (runReaderS s)

Can we use Yampa functions within? Turns out: yes

Yampa definitions:

.. code::

  type SF m = MSF (ClockInfo m)
  type ClockInfo m = ReaderT DTime m
  type DTime = Double

  SF :: Monad m => MSF (ReaderT Double m) a b

.. literalinclude:: ../src/myreactimate1.hs
  :emphasize-lines: 17

:download:`myreactimate1.hs <../src/myreactimate1.hs>`

>>> cabal repl myreactimate1 #--ghci-options '+RTS --io-manager=native -RTS'
>>> main

So if you disagree on how Yampa's ``reactimate`` is implemented, this is your chance. Let's get back to using `BearRiver.reactimate` though because it already works.


Terminating myreactimate
========================

.. pull-quote::

  [FrpRefac16]_ 4.3 Exceptions and Control Flow - MSFs can use different monads to define control structures. One common construct is switching, that is, applying a transformation until a certain time, and then applying a different transformation. We can implement an equivalent construct using monads like Either or Maybe. We could define a potentially-terminating MSF as an MSF in a MaybeT m monad. Following the same pattern as before, the associated running function would have type: runMaybeS :: Monad m => MSF (MaybeT m) a b -> MSF m a (Maybe b). Our evaluation function step, for this monad, would have type MSF Maybe a b -> a -> Maybe (b, MSF Maybe a b) indicating that it may produce no continuation. runMaybeS outputs Nothing continuously once the internal MSF produces no result.

Well, that doesn't help us much. Fortunately Dunai provides a ``reactimateB`` function which uses the ``ExceptT`` internally and allows us to quit using a ``Bool``.

.. literalinclude:: ../src/myreactimateB.hs
  :emphasize-lines: 29

:download:`myreactimateB.hs <../src/myreactimateB.hs>`

>>> cabal repl myreactimateB # --ghci-options '+RTS --io-manager=native -RTS'
>>> main

.. literalinclude:: ../src/myreactimateB1.hs
  :emphasize-lines: 28

:download:`myreactimateB1.hs <../src/myreactimateB1.hs>`

.. pull-quote::

  [FrpExt17]_ 6.4 Experience - Personally, I have found that using monad stacks with multiple transformers makes MSFs hard to work with, in spite of the multiple benefits of being able to describe switching in terms of EitherT or parallelism in terms of ListT. With this in mind, it is perhaps still convenient to add layers of abstraction on top of MSFs to describe Functional Reactive Programming or other mathematical constructs, as opposed to expressing programs directly in terms of Monadic Stream Functions (even if they are still implemented that way). Note, however, that programmers with a more mathematical background may find working with monad stacks more straightforward.

Okay...
