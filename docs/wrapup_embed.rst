.. code::
    
  :{
  do
    let yourMSF = count
    ls <- embed yourMSF ["input0", "input1", "input2"]
    print ls
  :}

  :{
  do
    let yourSF = integral
        dt = 1 / 60
    ls <- runReaderT (embed yourSF $ replicate 10 123.0) dt
    print ls
  :}

  :{
  do
    let yourSF = integral
        dt = 1 / 60
    ls <- embed (runReaderS_ yourSF dt) $ replicate 10 123.0
    print ls
  :}
