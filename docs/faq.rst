==========================
Frequently Asked Questions
==========================

Q: Are Arrows a bad design choice?

`Reddit - apfelmus on Yampa <https://www.reddit.com/r/haskell/comments/uuarz/comment/c4ymut6/?utm_source=reddit&utm_medium=web2x&context=3>`_ "In my opinion, Yampa's arrow style is very clunky to use, though.", "I don't perceive them as great. The main feature of arrows is that they restrict the flow of information (keyword: no strong monad), which is necessary for an efficient implementation of FRP, but the terrible price is that you have to do explicit plumbing. They don't solve the problem of restricting information flow in the usual applicative style, but that's exactly the problem I want to solve when I embed FRP into ordinary Haskell."

apfelmus is the implementor of reactive-banana and therefore his opinion matters on this topic.

`What I Wish I Knew When Learning Haskell - Arrows <http://dev.stephendiehl.com/hask/#arrows>`_ "In practice this notation is not often used and may become deprecated in the future." (citation required)
