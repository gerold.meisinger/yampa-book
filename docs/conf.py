# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))


# -- Project information -----------------------------------------------------

project = 'Yampa Book'
copyright = '2021, Gerold Meisinger'
author = 'Gerold Meisinger'


# -- General configuration ---------------------------------------------------

highlight_language = 'haskell'
suppress_warnings = ['ref.citation']

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
# ghc_packages https://gitlab.haskell.org/ghc/ghc/-/blob/master/docs/users_guide/ghc_packages.py https://gitlab.haskell.org/ghc/ghc/-/blob/master/docs/users_guide/conf.py
# sphinxcontrib.osexample https://github.com/svx/sphinxcontrib-osexample/issues/7
extensions = [
  'sphinx.ext.extlinks',
  'sphinx.ext.todo',
  ]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_rtd_theme'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

extlinks = {
  'hackage'    : ('https://hackage.haskell.org/package/%s'                                                , 'hackage %s'),
  'base'       : ('https://hackage.haskell.org/package/base/docs/%s.html'                                 , 'base %s'),
  'dunai'      : ('https://hackage.haskell.org/package/dunai/docs/Data-MonadicStreamFunction-Core.html#%s', 'dunai %s'),
  'dunai-util' : ('https://hackage.haskell.org/package/dunai/docs/Data-MonadicStreamFunction-Util.html#%s', 'dunai-util %s'),
  'dunai-trans': ('https://hackage.haskell.org/package/dunai/docs/Control-Monad-Trans-MSF-%t.html#%s'     , 'dunai-trans %s'),
  'bearriver'  : ('https://hackage.haskell.org/package/bearriver/docs/FRP-BearRiver.html#%s'              , 'bearriver %s'),
  'yampa'      : ('https://hackage.haskell.org/package/Yampa/docs/FRP-Yampa.html#%s'                      , 'yampa %s')
  }

todo_include_todos=True
