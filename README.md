# Yampa Book

Learn yourself a Yampa for great good!

https://yampa-book.rtfd.io

TODO: RTD badge/screenshot?

## Installation

TODO: literal include from rst
* it's a book, you can just read it
* if you want to follow along, see https://yampa-book.rtfd.io/en/latest/intro.html#installation

## Contributing

YES!
see https://yampa-book.rtfd.io/en/latest/contribute.html
* use issues
* fork and pull request

TODO: literal include from rst

## License

TODO: choose permissive license (for books)

## Project status

As of 2021-09 there are four chapters with examples but lots of todos and missing prose.

## Roadmap

- chapter "introduction"
  - [X] motivation
  - [ ] history
  - [ ] acknowledgements
  - [x] haskell
  - [x] installation
  - [x] contributing
  - [ ] more prose
- chapter "embed"
  - [X] basics
  - [X] reader
  - [X] writer
  - [ ] more prose
- chapter "reactimate"
  - [X] BearRiver
  - [X] Dunai
  - [X] terminating
  - [ ] more prose
- chapter "yampa"
  - [X] accumulating state
  - [ ] stateful functions
  - [X] animation
  - [X] movement
  - [X] recursion
  - [ ] diagrams for stateful functions
  - [ ] switches
  - [ ] object management
  - [ ] kinematics
  - [ ] more prose
- chapter "time"
  - [ ] discrete, continuous, hybrid
  - [ ] fixed rate, dynamic rate, mixed
  - [ ] faster, pause
  - [ ] subsampling
  - [ ] dynamic subsampling
  - [ ] frame rate, frame skip, frame monitor
  - [ ] custom integrals (runge cutta 4)
- chapter on "user interfaces"
  - [ ] unidirectional data flow
  - [ ] bidirectional data flow
  - [ ] tweens
  - [ ] stacked tweens
  - [ ] reversing stacked tweens midway
  - [ ] model state/animation state
  - [ ] draggable
- chapter "debugging"
  - [ ] logs
  - [ ] replaying inputs
  - [ ] wormholes, dynamic wormholes
  - [ ] back-in-time debugging
  - [ ] external monitoring
  - [ ] performance
  - [ ] performance optimizations
- chapter "real life"
  - [ ] integration in existing projects
  - [ ] FFI
  - [ ] Unity3D
  - [ ] Web
- chapter "examples"
  - [ ] time zones
  - [ ] abstract inputs: button, lever, stick, trigger, ball
  - [ ] input remapping
  - [ ] UI click states (dragStart/dragEnd)
  - [ ] SDL
  - [ ] OpenGL
- chapter "links"
  - [ ] Yampa papers
  - [ ] FRP papers
  - [ ] tutorials
  - [ ] videos
  - [ ] examples
  - [ ] libraries
- chapter "glossary"
  - [ ] cites of papers
  - [ ] add descriptions
- misc
  - [ ] readme
  - [ ] papers as RTD?
  - [ ] library docs as RTD?
  - [ ] Hoogle search bar?
  - [ ] "great good"-style illustrations :)
  - [ ] link to glossary terms
  - [ ] figure animations (animated svg)
  - [ ] add archive.org version to all links
  - [ ] implement common toy examples: Todo, CRUD,
