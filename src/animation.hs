{-# LANGUAGE Arrows #-}

import Data.Functor.Identity
import Data.IORef
import Data.Time.Clock
import FRP.BearRiver
import System.IO
--import System.Process (system)           -- alternative Windows clearScreen
--import System.Console.ANSI (clearScreen) -- alternative Linux console controls

-- a poor mans' console controls
hideCursor    = putStr  "\ESC[?25l"
clearScreen   = putStr  "\ESC[1J"
setCursor y x = putStr ("\ESC[" ++ show (max (y + 1) 1) ++ ";" ++ show (max (x + 1) 1) ++ "H")

inputInit :: IO (Maybe Char)
inputInit = do
  pure Nothing

input :: IORef UTCTime -> UTCTime -> a -> IO (DTime, Maybe (Maybe Char))
input dtRef tInit _ = do
  hasInput <- hWaitForInput stdin 100
  mc <- if hasInput then Just <$> getChar else pure Nothing

  now <- getCurrentTime
  prev <- readIORef dtRef
  writeIORef dtRef now
  let dt = realToFrac $ diffUTCTime now prev
  pure (dt, Just mc)

process :: SF Identity (Maybe Char) (Bool, String)
process = proc a -> do
  t <- time -< ()

  frame <- arr (\t -> ball !! (floor t `mod` length ball)) -< t

  let quit = Just 'q' == a
  returnA -< (quit, frame : "")
  where
    ball = "°Oo_oO"

output :: a -> (Bool, String) -> IO Bool
output _ (quit, frame) = do
  clearScreen
  --system "cls" -- alternative Windows clearScreen
  setCursor 0 0
  putStr frame
  pure quit

main = do
  putStrLn "Watch animation and press [Q] to quit!"

  hideCursor
  hSetBuffering stdin NoBuffering
  t <- getCurrentTime
  dtRef <- newIORef t

  reactimate inputInit (input dtRef t) output process

  putStrLn "...end"

-- > °Oo_oO°Oo_oO°Oo_oO (animating one frame per 100ms)