{-# LANGUAGE Arrows #-}

import Control.Concurrent (threadDelay)
import Data.Functor.Identity
import Data.IORef
import Data.Time.Clock
import FRP.BearRiver

inputInit :: IO String
inputInit = do
  pure "inputInit" -- use this if input doesn't produce anything

input :: IORef UTCTime -> UTCTime -> Bool -> IO (DTime, Maybe String)
input dtRef tInit _ = do
  now <- getCurrentTime
  prev <- readIORef dtRef
  writeIORef dtRef now
  let dt = realToFrac $ diffUTCTime now prev  -- delta time
  let at = realToFrac $ diffUTCTime now tInit -- absolute time
  return (dt, Just (show now)) -- usually you would want to use device inputs or resource media here but let's produce some input strings from time

sf :: Monad m => SF m String String
sf = proc nowStr -> do
  t <- integral -< 1.0 :: Double
  returnA -< " integral: " ++ show t ++ " now: " ++ nowStr

output :: Bool -> String -> IO Bool
output _ out = do
  putStrLn out
  threadDelay secs -- DON'T USE IT LIKE THIS IN A REAL GAME LOOP
  return False
  where secs = 1000 * 1000 -- pico seconds

main = do
  putStrLn "Warning: On Windows threadDelay only works without io-manager=native options!"
  putStrLn "Time progress in about 1sec (+ some processor time) steps. Interrupt with [Control-C]!"
  t <- getCurrentTime
  dtRef <- newIORef t

  reactimate inputInit (input dtRef t) output sf

  putStrLn "...end"

-- > start...
-- > now: 2021-09-12 09:42:57.1321897 UTC integral: 0.0
-- > now: 2021-09-12 09:42:58.1421375 UTC integral: 1.0099473
-- > now: 2021-09-12 09:42:59.1492352 UTC integral: 2.0170455
-- > Interrupted.
