{-# LANGUAGE Arrows #-}

import FRP.BearRiver
import Data.IORef
import Data.Time.Clock
import Data.Functor.Identity
import Numeric
import System.IO
import Data.Char

inputInit :: IO Char
inputInit = do
  pure '\x00'

input :: IORef UTCTime -> a -> IO (DTime, Maybe Char)
input dtRef _ = do
  c <- getChar

  now <- getCurrentTime
  prev <- readIORef dtRef
  writeIORef dtRef now
  let dt = realToFrac $ diffUTCTime now prev  -- delta time
  pure (dt, Just c)

process :: SF Identity Char (Bool, String, String)
process = proc i -> do
  t <- time -< ()

  let n = if isDigit i then digitToInt i else 0
  c <- sumS -< fromIntegral n :: Double -- required for VectorSpace
  -- c <- feedback 0 (arr feedbackAdd) -< n
  -- c <- stateful 0 statefulAdd -< n
  -- let nE = if isDigit i then Event (digitToInt i) else NoEvent
  -- c <- accumHoldBy accumAdd 0 -< nE

  returnA -< (i == 'q', showFFloat (Just 2) t "", show c)
    where
      -- feedbackAdd (accu, new) = dup (accu + new)
      -- statefulAdd accu new = accu + new
      -- accumAdd new accu = accu + new
      -- stateful :: Monad m => b -> (a -> b -> b) -> SF m a b
      -- stateful bInit f = proc a -> do
      --   b' <- feedback bInit (arr (\(a, b) -> dup $ f a b)) -< a
      --   returnA -< b'

output :: a -> (Bool, String, String) -> IO Bool
output _ (quit, timeStr, countStr) = do
  putStrLn $ "time: " ++ timeStr ++ " counter: " ++ countStr
  pure quit

main = do
  putStrLn "Enter some chars to tick, digits to add or quit with [Q]!"
  hSetBuffering stdin NoBuffering
  t <- getCurrentTime
  dtRef <- newIORef t

  reactimate inputInit (input dtRef) output process

  putStrLn "...end"
