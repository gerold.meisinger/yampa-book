{-# LANGUAGE Arrows #-}

import FRP.BearRiver
import Control.Monad.Reader
import Data.IORef
import Data.Time.Clock
import Data.Functor.Identity
import System.IO
import Data.Char
import Data.Functor
--import System.Process (system)           -- alternative Windows clearScreen
--import System.Console.ANSI (clearScreen) -- alternative Linux console controls

-- textfield
data TextfieldInEvents = TextfieldInEvents
  { setText    :: Event String
  , keyPressed :: KeyPressed
  }
data TextfieldOutEvents = TextfieldOutEvents
  { textChanged :: Event String }
type TextfieldState = (String, Int, Char) -- text, cursorPos, cursorFrame

keyLeft  = ','
keyRight = '.'
keyBack  = '-'
keyQuit  = '0'
keysFire = "123456789"
cursorFrames = "^^^^ "

-- https://apfelmus.nfshost.com/blog/2012/03/29-frp-three-principles-bidirectional-gui.html
-- 1. Only the program may manipulate the output display, never the user.
-- 2. User input is presented in the form of events.
-- 3. GUI elements generate events only in response to user input, never in response to program output.
textfield :: String -> SF Identity TextfieldInEvents (TextfieldState, TextfieldOutEvents)
textfield textInit = proc events -> do
  (text, cursorPos, textChanged) <- accumulateWith handleEvents (textInit, cursorPosInit, NoEvent) -< events
  cursorFrame <- animate cursorFrames 5.0 -< ()
  returnA -< ((text, cursorPos, cursorFrame), TextfieldOutEvents textChanged)
  where
    cursorPosInit = length textInit
    handleEvents :: TextfieldInEvents -> (String, Int, Event String) -> (String, Int, Event String)
    handleEvents TextfieldInEvents { setText    = Event text } (_   , cursorPos, _) = (text, min cursorPos (length text), NoEvent)
    handleEvents TextfieldInEvents { keyPressed = Event key  } (text, cursorPos, _) = case key of
      key | key == keyBack  -> if cursorPos > 0 then let textNew = removeAt text (cursorPos - 1) in (textNew, cursorPos - 1, Event textNew) else (text, cursorPos, NoEvent)
      key | key == keyLeft  -> (text, max (cursorPos - 1) 0, NoEvent)
      key | key == keyRight -> (text, min (cursorPos + 1) (length text), NoEvent)
      _                     -> let textNew = insertAt text cursorPos key in (textNew, cursorPos + 1, Event textNew)
    handleEvents _ old = old

outputTextfield :: TextfieldState -> IO ()
outputTextfield (text, cursorPos, cursorFrame) = do
  putStr text
  setCursor 1 cursorPos
  putChar cursorFrame

-- a poor mans' console controls
hideCursor    = putStr "\ESC[?25l"
setCursor y x = putStr ("\ESC[" ++ show (max (y + 1) 1) ++ ";" ++ show (max (x + 1) 1) ++ "H")
clearScreen   = putStr "\ESC[1J" >> setCursor 0 0

-- string utilities
removeAt s i = take i s ++ drop (i + 1) s
insertAt s i c = take i s ++ c : drop i s

-- animation utilities
animate :: [a] -> Double -> SF Identity () a
animate frames speed = constant speed >>> integral >>^ getFrame frames

getFrame :: [a] -> Double -> a
getFrame frames t = let n = length frames in frames !! (floor t `mod` n)

-- engine
type KeyPressed = Event Char

inputInit :: IO KeyPressed
inputInit = do
  pure NoEvent

input :: IORef UTCTime -> UTCTime -> a -> IO (DTime, Maybe KeyPressed)
input dtRef tInit _ = do
  hasInput <- hWaitForInput stdin 100
  mc <- if hasInput then Event <$> getChar else pure NoEvent

  now  <- getCurrentTime
  prev <- readIORef dtRef
  writeIORef dtRef now
  let dt = realToFrac $ diffUTCTime now prev  -- delta time

  pure (dt, Just mc)

process :: SF Identity KeyPressed (Bool, TextfieldState)
process = proc i -> do
  (textfield0, textChanged0) <- textfield "Hello world" -< TextfieldInEvents { setText = i >>= \c -> if c `elem` keysFire then Event (replicate (digitToInt c) 'X') else NoEvent, keyPressed = i }
  let doQuit = keyQuit `elem` i
  returnA -< (doQuit, textfield0)

output :: a -> (Bool, TextfieldState) -> IO Bool
output _ (doQuit, textfield) = do
  clearScreen
  outputTextfield textfield
  pure doQuit

main = do
  clearScreen
  putStrLn $ "Enter chars\nMove cursor [" ++ keyLeft : "]/[" ++ keyRight : "]\nDelete char [" ++ keyBack : "]\nFire setText event [" ++ keysFire ++ "]\nQuit with [" ++ keyQuit : "]\nPress any key to start..."
  getChar

  hideCursor
  hSetBuffering stdin NoBuffering
  t <- getCurrentTime
  dtRef <- newIORef t

  reactimate inputInit (input dtRef t) output process

  clearScreen
  putStrLn "...end"
