import Control.Monad
import Control.Monad.Trans
import Control.Monad.Trans.MSF.Reader
import Control.Monad.Trans.MSF.Writer
import FRP.BearRiver

type Game = Ball
type Ball = Int

data GameSettings = GameSettings
  { leftPlayerPos  :: Int
  , rightPlayerPos :: Int
  }

--type GameEnv m = ReaderT GameSettings m
--type GameEnv m = WriterT [String] (ReaderT GameSettings m)
type GameEnv m = ReaderT GameSettings (WriterT [String] m)

ballToRight :: Monad m => MSF (GameEnv m) () Ball
ballToRight = count >>> arrM (\n -> (n+) <$> asks leftPlayerPos) >>> arrM checkHitR
--ballToRight = count >>> arrM (\n -> (n+) <$> asks leftPlayerPos) >>> withSideEffect checkHitR
  where
    checkHitR :: Monad m => Int -> GameEnv m Int -- change Int to () when using withSideEffect
    checkHitR n = do
      rp <- asks rightPlayerPos
      lift $ when (rp > n) $ tell ["Ball at " ++ show n]
      pure n -- remove when using withSideEffect

hitRight :: Monad m => MSF (GameEnv m) Ball Bool
hitRight = arrM $ \i -> (i >=) <$> asks rightPlayerPos

testMSF = ballToRight >>> (arr id &&& hitRight)

main = do
  embed (runWriterS (runReaderS_ testMSF (GameSettings 0 3))) (replicate 5 ())
