{-# LANGUAGE Arrows #-}

import FRP.BearRiver
import Data.IORef
import Data.Time.Clock
import Data.Functor.Identity
import System.IO

ball  = "°Oo_oO"

hideCursor    = putStr  "\ESC[?25l"
clearScreen   = putStr  "\ESC[1J"
setCursor y x = putStr ("\ESC[" ++ show (max (y + 1) 1) ++ ";" ++ show (max (x + 1) 1) ++ "H")

animate :: [a] -> Double -> SF Identity () a
animate frames speed = constant speed >>> integral >>^ getFrame frames

getFrame :: [a] -> Double -> a
getFrame frames t = let n = length frames in frames !! (floor t `mod` n)

inputInit :: IO (Maybe Char)
inputInit = do
  pure Nothing

input :: IORef UTCTime -> UTCTime -> a -> IO (DTime, Maybe (Maybe Char))
input dtRef tInit _ = do
  hasInput <- hWaitForInput stdin 100
  mc <- if hasInput then Just <$> getChar else pure Nothing

  now <- getCurrentTime
  prev <- readIORef dtRef
  writeIORef dtRef now
  let dt = realToFrac $ diffUTCTime now prev
  pure (dt, Just mc)

process :: SF Identity (Maybe Char) (Bool, (Int, Int, String))
process = proc a -> do
  obj <- object -< a
  let quit = Just 'q' == a
  returnA -< (quit, obj)

object :: SF Identity (Maybe Char) (Int, Int, String)
object = proc a -> do
  x <- accumHoldBy (+) 0 -< case a of Just 'd' -> Event 1; Just 'a' -> Event (-1); _ -> NoEvent
  y <- accumHoldBy (+) 0 -< case a of Just 's' -> Event 1; Just 'w' -> Event (-1); _ -> NoEvent
  frame <- animate ball 5.0 -< ()
  returnA -< (x, y, frame : "")

output :: a -> (Bool, (Int, Int, String)) -> IO Bool
output _ (quit, (x, y, frame)) = do
  clearScreen
  setCursor y x
  putStr frame
  pure quit

main = do
  putStrLn "Press [WASD] to move around and [Q] to quit!"
  hideCursor
  hSetBuffering stdin NoBuffering
  t <- getCurrentTime
  dtRef <- newIORef t
  hasInput <- hWaitForInput stdin 5000

  reactimate inputInit (input dtRef t) output process

  clearScreen
  putStrLn "...end"
