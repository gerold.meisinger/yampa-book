import Control.Monad.Trans.MSF.Reader
import Data.MonadicStreamFunction

type Game = Ball
type Ball = Int

data GameSettings = GameSettings
  { leftPlayerPos  :: Int
  , rightPlayerPos :: Int
  }

type GameEnv m = ReaderT GameSettings m

ballToRight :: Monad m => MSF (GameEnv m) () Ball
ballToRight = count >>> arrM (\n -> (n+) <$> asks leftPlayerPos)

hitRight :: Monad m => MSF (GameEnv m) Ball Bool
hitRight = arrM $ \i -> (i >=) <$> asks rightPlayerPos

testMSF = ballToRight >>> (arr id &&& hitRight)

main :: IO [((Ball, Bool), (Ball, Bool))]
main = do
  embed (runReaderS_ testMSF (GameSettings 0 3)) (replicate 5 ())
  -- > [(1,False),(2,False),(3,True),(4,True),(5,True)]
  embed (runReaderS_ testMSF (GameSettings 0 2)) (replicate 5 ())
  -- > [(1,False),(2,True),(3,True),(4,True),(5,True)]
  embed (runReaderS_ testMSF (GameSettings 0 3) &&& runReaderS_ testMSF (GameSettings 0 2)) (replicate 5 ())
  -- > [((1,False),(1,False)),((2,False),(2,True)),((3,True),(3,True)),((4,True),(4,True)),((5,True),(5,True))]
