{-# LANGUAGE Arrows #-}

import FRP.BearRiver
import Data.IORef
import Data.Time.Clock
import Data.Functor.Identity
import System.IO
import Data.Char
--import System.Process (system)           -- alternative Windows clearScreen
--import System.Console.ANSI (clearScreen) -- alternative Linux console controls

-- textfield
type Textfield = (String, Int, String) -- text, cursorPos, cursorFrame

keyLeft  = ','
keyRight = '.'
keyBack  = '-'
keyQuit  = '0'
keysFire = "123456789"
cursorFrames = "^^^^ "

textfield :: String -> SF Identity (KeyPressed, Event String) Textfield
textfield textInit = proc (keyPress, setText) -> do
  rec
    (text, cursorPos) <- iPre (textInit, posInit) <<< accumHoldBy foo (textInit, posInit) -< (setText `attach` cursorPos) `lMerge`
      case keyPress of
        Event c | c == keyBack  -> Event (if cursorPos > 0 then take (cursorPos - 1) text ++ drop cursorPos text else text, cursorPos - 1)
        Event c | c == keyLeft  -> Event (text, cursorPos - 1)
        Event c | c == keyRight -> Event (text, cursorPos + 1)
        _ -> keyPress >>= \c    -> Event (take cursorPos text ++ c : drop cursorPos text, cursorPos + 1)

  cursorFrame <- animate cursorFrames 5.0 -< ()
  returnA -< (text, cursorPos, cursorFrame : "")
  where
    foo = \(textOld, posOld) (textNew, posNew) -> (textNew, max 0 . min (length textNew) $ posNew)
    posInit = length textInit

outputTextfield :: Textfield -> IO ()
outputTextfield (text, cursorPos, cursorFrame) = do
  putStr text
  setCursor 1 cursorPos
  putStr cursorFrame

-- a poor mans' console controls
hideCursor    = putStr "\ESC[?25l"
setCursor y x = putStr ("\ESC[" ++ show (max (y + 1) 1) ++ ";" ++ show (max (x + 1) 1) ++ "H")
clearScreen   = putStr "\ESC[1J" >> setCursor 0 0

-- animation utilities
animate :: [a] -> Double -> SF Identity () a
animate frames speed = constant speed >>> integral >>^ getFrame frames

getFrame :: [a] -> Double -> a
getFrame frames t = let n = length frames in frames !! (floor t `mod` n)

-- engine
type KeyPressed = Event Char

inputInit :: IO KeyPressed
inputInit = do
  pure NoEvent

input :: IORef UTCTime -> UTCTime -> a -> IO (DTime, Maybe KeyPressed)
input dtRef tInit _ = do
  hasInput <- hWaitForInput stdin 100
  mc <- if hasInput then Event <$> getChar else pure NoEvent

  now  <- getCurrentTime
  prev <- readIORef dtRef
  writeIORef dtRef now
  let dt = realToFrac $ diffUTCTime now prev  -- delta time

  pure (dt, Just mc)

process :: SF Identity KeyPressed (Bool, Textfield)
process = proc i -> do
  tf <- textfield "Hello world" -< (i, i >>= \c -> if c `elem` keysFire then Event (replicate (digitToInt c) 'X') else NoEvent)
  let doQuit = Event keyQuit == i
  returnA -< (doQuit, tf)

output :: a -> (Bool, Textfield) -> IO Bool
output _ (doQuit, textfield) = do
  clearScreen
  outputTextfield textfield
  pure doQuit

main = do
  clearScreen
  putStrLn $ "Enter chars\nMove cursor [" ++ keyLeft : "]/[" ++ keyRight : "]\nDelete char [" ++ keyBack : "]\nFire setText event [" ++ keysFire ++ "]\nQuit with [" ++ keyQuit : "]\nPress any key to start..."
  getChar

  hideCursor
  hSetBuffering stdin NoBuffering
  t <- getCurrentTime
  dtRef <- newIORef t

  reactimate inputInit (input dtRef t) output process

  clearScreen
  putStrLn "...end"
