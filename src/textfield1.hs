{-# LANGUAGE Arrows #-}

import FRP.BearRiver
import Control.Monad.Reader
import Data.IORef
import Data.Time.Clock
import Data.Functor.Identity
import System.IO
import Data.Char
import Data.Functor
--import System.Process (system)           -- alternative Windows clearScreen
--import System.Console.ANSI (clearScreen) -- alternative Linux console controls

-- textfield
type Textfield = (String, Int, Char) -- text, cursorPos, cursorFrame

keyLeft  = ','
keyRight = '.'
keyBack  = '-'
keyQuit  = '0'
keysFire = "123456789"
cursorFrames = "^^^^ "

-- https://apfelmus.nfshost.com/blog/2012/03/29-frp-three-principles-bidirectional-gui.html
-- 1. Only the program may manipulate the output display, never the user.
-- 2. User input is presented in the form of events.
-- 3. GUI elements generate events only in response to user input, never in response to program output.
textfield :: String -> SF Identity (KeyPressed, Event String) Textfield
textfield textInit = proc (keyPress, setText) -> do
  let
    backE  = filterE (== keyBack ) keyPress
    leftE  = filterE (== keyLeft ) keyPress
    rightE = filterE (== keyRight) keyPress
    charE  = if isNoEvent $ mergeEvents [backE, leftE, rightE] then keyPress else NoEvent

  rec
    let handleBack = backE `tag` (if cursorPosOld > 0 then removeAt textOld cursorPosOld else textOld)
        handleChar = charE <&> insertAt textOld cursorPosOld
        limitPos p = min (length textNew) . max 0 $ p
    textNew      <- hold textInit -< mergeEvents [setText, handleBack, handleChar]
    cursorPosNew <- hold posInit  -< mergeEvents
      [ setText `tag`  cursorPosOld
      , backE   `tag` (cursorPosOld - 1)
      , leftE   `tag` (cursorPosOld - 1)
      , rightE  `tag` (cursorPosOld + 1)
      , charE   `tag` (cursorPosOld + 1)
      ] <&> limitPos
    textOld      <- iPre textInit -< textNew
    cursorPosOld <- iPre posInit  -< cursorPosNew

  cursorFrame <- animate cursorFrames 5.0 -< ()
  returnA -< (textNew, cursorPosNew, cursorFrame)
  where
    posInit = length textInit

outputTextfield :: Textfield -> IO ()
outputTextfield (text, cursorPos, cursorFrame) = do
  putStr text
  setCursor 1 cursorPos
  putChar cursorFrame

-- a poor mans' console controls
hideCursor    = putStr "\ESC[?25l"
setCursor y x = putStr ("\ESC[" ++ show (max (y + 1) 1) ++ ";" ++ show (max (x + 1) 1) ++ "H")
clearScreen   = putStr "\ESC[1J" >> setCursor 0 0

-- string utilities
removeAt s i = take (i - 1) s ++ drop i s
insertAt s i c = take i s ++ c : drop i s

-- animation utilities
animate :: [a] -> Double -> SF Identity () a
animate frames speed = constant speed >>> integral >>^ getFrame frames

getFrame :: [a] -> Double -> a
getFrame frames t = let n = length frames in frames !! (floor t `mod` n)

-- engine
type KeyPressed = Event Char

inputInit :: IO KeyPressed
inputInit = do
  pure NoEvent

input :: IORef UTCTime -> UTCTime -> a -> IO (DTime, Maybe KeyPressed)
input dtRef tInit _ = do
  hasInput <- hWaitForInput stdin 100
  mc <- if hasInput then Event <$> getChar else pure NoEvent

  now  <- getCurrentTime
  prev <- readIORef dtRef
  writeIORef dtRef now
  let dt = realToFrac $ diffUTCTime now prev  -- delta time

  pure (dt, Just mc)

process :: SF Identity KeyPressed (Bool, Textfield)
process = proc i -> do
  tf <- textfield "Hello world" -< (i, i >>= \c -> if c `elem` keysFire then Event (replicate (digitToInt c) 'X') else NoEvent)
  let doQuit = Event keyQuit == i
  returnA -< (doQuit, tf)

output :: a -> (Bool, Textfield) -> IO Bool
output _ (doQuit, textfield) = do
  clearScreen
  outputTextfield textfield
  pure doQuit

main = do
  clearScreen
  putStrLn $ "Enter chars\nMove cursor [" ++ keyLeft : "]/[" ++ keyRight : "]\nDelete char [" ++ keyBack : "]\nFire setText event [" ++ keysFire ++ "]\nQuit with [" ++ keyQuit : "]\nPress any key to start..."
  getChar

  hideCursor
  hSetBuffering stdin NoBuffering
  t <- getCurrentTime
  dtRef <- newIORef t

  reactimate inputInit (input dtRef t) output process

  clearScreen
  putStrLn "...end"
