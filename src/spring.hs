{-# LANGUAGE Arrows #-}

import Control.Monad
import Control.Monad.Fix
import Control.Monad.Trans
import Control.Monad.Trans.MSF.Reader
import Control.Monad.Trans.MSF.Writer
import Data.Functor.Identity
import Data.IORef
import Data.Text.Chart (plot)
import Data.Time.Clock
import FRP.BearRiver
import Numeric
import System.IO

hideCursor    = putStr  "\ESC[?25l"
clearScreen   = putStr  "\ESC[1J"
setCursor y x = putStr ("\ESC[" ++ show (max (y + 1) 1) ++ ";" ++ show (max (x + 1) 1) ++ "H")

inputInit :: IO (Maybe Char)
inputInit = do
  pure Nothing

input :: IORef UTCTime -> IORef Bool -> a -> IO (DTime, Maybe (Maybe Char))
input dtRef quitRef _ = do
  hasInput <- hWaitForInput stdin 100
  mc <- if hasInput then Just <$> getChar else pure Nothing
  when (mc == Just 'q') $ writeIORef quitRef True

  now <- getCurrentTime
  prev <- readIORef dtRef
  writeIORef dtRef now
  let dt = realToFrac $ diffUTCTime now prev
  pure (dt, Just mc)

output :: IORef Bool -> a -> Double -> IO Bool
output quitRef _ x = do
  clearScreen
  setCursor 0 0
  putStr $ replicate (round x) '~' ++ "O"
  readIORef quitRef

spring :: Monad m => Double -> Double -> Double -> SF m a Double
spring k x0 xdInit = feedback xInit $ proc (_, xt) -> do
  let xd = x0 - xt
  ft  <- integralFrom 0     -< xd
  xt' <- integralFrom xInit -< ft / k
  returnA -< (xt', xt')
  where 
    xInit = x0 + xdInit

main = do
  putStrLn "Watch an animated spring and press [Q] to quit!"
  hasInput <- hWaitForInput stdin 5000
  hSetBuffering stdin NoBuffering
  hideCursor
  t <- getCurrentTime
  dtRef <- newIORef t
  quitRef <- newIORef False

  reactimate inputInit (input dtRef quitRef) (output quitRef) (spring k x0 xdInit)

  clearScreen
  setCursor 0 0
  putStrLn "Time-distance diagram (using ASCII plot):"
  ls <- embed (runReaderS_ (spring k x0 xdInit) 0.1) (replicate 120 ())
  plot $ map round ls

  putStrLn "...end"
  where
    k      =  1.0
    x0     = 30.0
    xdInit = 20.0

-- > Watch an animated spring and press [Q] to quit!
-- > ~~~~~~~~~~~~~~~~~~~~O (bouncing back and forth)
-- > Time-distance diagram (using Unicode plot):
-- > 50.00 ┼╮                                                          ╭───╮                                                         ╭────╮
-- > 47.14 ┤╰──╮                                                    ╭──╯   ╰──╮                                                    ╭─╯    ╰──╮  
-- > 44.29 ┤   ╰──╮                                              ╭──╯         ╰─╮                                               ╭──╯         ╰─╮
-- > 41.43 ┤      ╰─╮                                          ╭─╯              ╰─╮                                           ╭─╯              ╰─╮  
-- > 38.57 ┤        ╰╮                                       ╭─╯                  ╰─╮                                       ╭─╯                  ╰─╮
-- > 35.71 ┤         ╰─╮                                    ╭╯                      ╰─╮                                    ╭╯                      ╰─╮ 
-- > 32.86 ┤           ╰╮                                 ╭─╯                         ╰╮                                 ╭─╯                         ╰╮
-- > 30.00 ┤            ╰╮                               ╭╯                            ╰╮                               ╭╯                            ╰╮  
-- > 27.14 ┤             ╰─╮                            ╭╯                              ╰─╮                            ╭╯                              ╰─╮
-- > 24.29 ┤               ╰╮                         ╭─╯                                 ╰╮                         ╭─╯                                 ╰╮  
-- > 21.43 ┤                ╰─╮                      ╭╯                                    ╰─╮                      ╭╯                                    ╰─╮
-- > 18.57 ┤                  ╰─╮                  ╭─╯                                       ╰─╮                  ╭─╯                                       ╰╮  
-- > 15.71 ┤                    ╰─╮              ╭─╯                                           ╰─╮              ╭─╯                                          ╰─╮
-- > 12.86 ┤                      ╰─╮         ╭──╯                                               ╰─╮         ╭──╯                                              ╰─
-- > 10.00 ┤                        ╰─────────╯                                                    ╰─────────╯
-- > ...end
