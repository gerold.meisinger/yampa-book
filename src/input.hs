{-# LANGUAGE Arrows #-}

import Data.Functor.Identity
import Data.IORef
import Data.Time.Clock
import FRP.BearRiver
import Numeric
import System.IO ( stdin, hSetBuffering, BufferMode(NoBuffering) )

inputInit :: IO (Char, String)
inputInit = do
  pure ('\x00', "inputInit")

input :: IORef UTCTime -> UTCTime -> a -> IO (DTime, Maybe (Char, String))
input dtRef tInit _ = do
  c <- getChar

  now <- getCurrentTime
  prev <- readIORef dtRef
  writeIORef dtRef now
  let dt = realToFrac $ diffUTCTime now prev  -- delta time
  let at = realToFrac $ diffUTCTime now tInit -- absolute time
  pure (dt, Just (c, show now))

sf :: SF Identity (Char, String) (Bool, String)
sf = proc (input, nowStr) -> do
  t <- integral -< 1.0 :: Double
  let out = "integral: " ++ showFFloat (Just 2) t "" ++ " now: " ++ nowStr
  returnA -< (input == 'q', out)

output :: a -> (Bool, String) -> IO Bool
output _ (quit, out) = do
  putStrLn out
  pure quit

main = do
  putStrLn "Repeatedly press any key or [Q] to quit!"

  hSetBuffering stdin NoBuffering
  t <- getCurrentTime
  dtRef <- newIORef t

  reactimate inputInit (input dtRef t) output sf

  putStrLn "...end"

-- > start...
-- > Repeatedly press any key or [Q] to quit!
-- > integral: 0.64 now: 2021-09-12 10:09:21.9324477 UTC
-- > integral: 1.80 now: 2021-09-12 10:09:23.0927901 UTC
-- > integral: 2.73 now: 2021-09-12 10:09:24.0177477 UTC
-- > ...end
