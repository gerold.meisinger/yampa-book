{-# LANGUAGE Arrows #-}

import Control.Monad.Trans.MSF.Except (reactimateB)
import Control.Monad.Trans.MSF.Reader
import Data.Functor.Identity
import Data.MonadicStreamFunction
import System.IO

type SF m = MSF (ReaderT Double m)

input :: IO (Double, Char)
input = do
  i <- getChar
  pure (dt, i)
  where dt = 1.0

process :: SF Identity Char (Bool, String)
process = proc i -> do                -- input is inserted as an arrow input
  tStr <- constM (show <$> ask) -< () -- whereas the time deltas are provided from the reader monad
  returnA -< (i == 'q', tStr)

output :: (Bool, String) -> IO Bool
output (quit, o) = do
  putStrLn o
  pure quit
  -- we could also move the quit logic from process to output directly

myreactimate :: IO (Double, a) -> (b -> IO Bool) -> SF Identity a b -> IO ()
myreactimate sense actuate sf = reactimateB $ senseSF sense >>> sfIO sf >>> actuateSF actuate
  where
    senseSF :: Monad m => m a -> MSF m () a
    senseSF s = constM s
    actuateSF :: Monad m => (a -> m b) -> MSF m a b
    actuateSF a = arrM a
    sfIO :: Monad m2 => MSF (ReaderT r Identity) a b -> MSF m2 (r, a) b
    sfIO s = morphS (pure . runIdentity) (runReaderS s)

main = do
  putStrLn "Press any key to get fake delta times and [Q] to quit!"
  hSetBuffering stdin NoBuffering
  myreactimate input output process
