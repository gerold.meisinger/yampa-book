{-# LANGUAGE Arrows, ScopedTypeVariables #-}

import Control.Monad.Trans.MSF.Reader
import Data.Functor.Identity
import Data.MonadicStreamFunction
import FRP.BearRiver hiding (reactimate)
import System.IO

input :: IO (Double, Char)
input = do
  i <- getChar
  pure (dt, i)
  where dt = 1.0

process :: SF Identity Char String -- this is an BearRiver.SF now, not a Dunai.MSF
process = proc i -> do
  t <- sumS -< 1.0 :: Double -- using BearRiver.sumS here!
  returnA -< show t

output :: String -> IO ()
output o = putStrLn o

myreactimate :: forall a b. IO (Double, a) -> (b -> IO ()) -> SF Identity a b -> IO ()
myreactimate sense actuate sf = reactimate $ senseSF >>> sfIO >>> actuateSF
  where
    senseSF :: MSF IO () (Double, a)
    senseSF = constM sense
    actuateSF :: MSF IO b ()
    actuateSF = arrM actuate
    sfIO :: MSF IO (Double, a) b
    sfIO = morphS (pure . runIdentity) (runReaderS sf)

main :: IO ()
main = do
  putStrLn "Press any key to get custom process counts and [Control-C] to interrupt!"
  hSetBuffering stdin NoBuffering
  myreactimate input output process
